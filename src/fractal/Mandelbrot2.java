
package fractal;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

/**
 *
 * @author NISI
 */
public class Mandelbrot2 extends JFrame {
 
    private final int MAX_ITER = 570;
    private final double ZOOM = 200;
    private BufferedImage I;
    private double zx, zy, cX, cY, tmp;
 
    public Mandelbrot2() {
        super("Mandelbrot Set");
        setBounds(100, 100, 800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        I = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                zx = zy = 0;
                cX = (x - 400) / ZOOM;
                cY = (y - 300) / ZOOM;
                int iter = MAX_ITER;
                while (Math.pow(zx,2) + Math.pow(zy, 2) < 4 && iter > 0){ 
                    
                    tmp= Math.pow(zx,4) - 6 * Math.pow(zx,2) * Math.pow(zy,2)+ Math.pow(zy,4)+ cX;
                    zy=4* Math.pow(zx,3)* zy -4*zx* Math.pow(zy,3)+ cY;
                    zx = tmp;  
                    
                    iter--;
                }
                I.setRGB(x, y, iter | (iter << 255));
            }
        }
    }
 
    @Override   
    public void paint(Graphics g) {
        g.drawImage(I, 0, 0, this);
    }
 
    public static void main(String[] args) {
        new Mandelbrot2().setVisible(true);
    }
}
